import express from 'express';
import { initKeycloak, getKeycloak } from '../config/keycloak-config.js';
import { getUsersRouter } from './routers/usersRouter.js';

class App {

    constructor() {
        const app = express();

        // Protect server with keycloak
        initKeycloak(app);
        app.use(getKeycloak().middleware())

        app.use(express.json());
        app.set('json spaces', 8);
        app.use('/api/users', getUsersRouter());
        this.app = app;
    }

    setOnReady(callback) {
        this.app.on('app_ready', callback);
    }

    start(port) {
        if (!port) {
            port = 0;
        }

        const server = this.app.listen(port, () => {
            const actualPort = server.address().port;
            this.app.emit("app_ready", actualPort);
        })
    }
}

export default App;