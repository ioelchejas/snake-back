import express from 'express';
import { getKeycloak } from '../../config/keycloak-config.js';
import { User } from '../models/user.js';

function getUsersRouter() {

    const router = express.Router();

    router.post('/', async(req, res) => {
        const user = new User(req.body);
        try {
            await user.save();
            res.send(user);
        } catch (error) {
            res.status(500).send(error);
        }
    })

    router.get('/', getKeycloak().protect(), async(req, res) => {
        const users = await User.find({});
        try {
            res.send(users);
        } catch (error) {
            res.status(500).send(error);
        }
    })

    router.get('/:id', async(req, res) => {
        const user = await User.findOne({ _id: req.params.id });
        try {
            res.send(user);
        } catch (error) {
            res.status(500).send(error);
        }
    })

    router.delete('/:id', async(req, res) => {
        await User.deleteOne({ _id: req.params.id })
        try {
            res.status(204).send("Usuario borrado con exito!");
        } catch (err) {
            res.status(400).json(err);
        }
    })

    router.put('/:id', async(req, res) => {
        const user = await User.findOneAndUpdate({ _id: req.params.id }, req.body)
        try {
            res.send(user);
        } catch (err) {
            res.status(400).json(err);
        }
    })

    return router
}
export { getUsersRouter }