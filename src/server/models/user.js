import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
    age: {
        type: Number,
        required: true,
        validate(value) {
            if (value < 0) throw new Error("Negative age aren't real.");
        },
    },
});

const User = mongoose.model("user", UserSchema);

export { User };