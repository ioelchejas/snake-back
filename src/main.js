import Servidor from './server/app.js';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
dotenv.config();

const app = new Servidor();

const PORT = process.env.PORT || 8080;

// MSJ AL INICIAR SERVER
app.setOnReady(async(port) => {
    console.log(`listening on port: ${port}`);
})

app.start(PORT);

mongoose.connect(
    "mongodb+srv://ioelchejas:12345@cluster0.iwau3.mongodb.net/usersdb?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        //  useFindAndModify: false,
        useUnifiedTopology: true
    }
).then(() => { console.log("succesfully conected to database") });