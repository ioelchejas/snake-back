import session from 'express-session';
import Keycloak from 'keycloak-connect';

let _keycloak;

const keycloakConfig = {
    clientId: 'snake-auth',
    bearerOnly: true,
    serverUrl: 'https://snake-auth.herokuapp.com/auth',
    realm: 'auth-realm',
    credentials: {
        secret: '62c99f7c-da55-48fb-ae4e-a27f132546b7'
    }
};

function initKeycloak(app) {
    if (_keycloak) {
        console.warn("Trying to init Keycloak again!");
        return _keycloak;
    } else {
        console.log("Initializing Keycloak...");
        var memoryStore = new session.MemoryStore();
        app.use(session({
            secret: '62c99f7c-da55-48fb-ae4e-a27f132546b7',
            resave: false,
            saveUninitialized: true,
            store: memoryStore
        }));
        _keycloak = new Keycloak({ store: memoryStore }, keycloakConfig);
        return _keycloak;
    }
}

function getKeycloak() {
    if (!_keycloak) {
        console.error('Keycloak has not been initialized. Please called init first.');
    }
    return _keycloak;
}

export {
    initKeycloak,
    getKeycloak
}